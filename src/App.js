import React from 'react';
// import logo from './logo.svg';
import './App.css';
import { Container } from '@material-ui/core';

class  App extends React.Component{

    constructor() {
        super()
        this.state = {
            name: '',
            description: '',
            category: '',
            price: '',

            errors: {
                name: '',
                category: '',
                price: '',
            },

            productList: [],
            tempEditProductIndex: null,   // temporary edit product index

            categories: [
                {
                    id: 1,
                    name: 'Mobile'
                },
                {
                    id: 2,
                    name: 'Laptop'
                },
                {
                    id: 3,
                    name: 'Storage device'
                },
                {
                    id: 4,
                    name: 'Television'
                }
            ]
        }
    }

    changeHandler = (event) => {
        event.preventDefault();

        // set input value
        const target = event.target;
        this.setState({
            [target.name]: target.value
        })
    }

    // validateForm = (errors) => {
    validateForm = () => {
        let valid = true;
        let errors = this.state.errors;

        errors.name = (this.state.name.length === 0) ? 'Name is required!' : '';

        errors.category = (this.state.category.length === 0) ? 'Category is required!' : '';

        if(this.state.price.length === 0) {
            errors.price = 'Price is required!';
        } else {
            if(!this.state.price.match(/^[0-9.]+$/)) {
                errors.price = 'Price should be numeric!';
            } else {
                errors.price = '';
            }
        }

        this.setState({errors});

        // if we have an error string set valid to false
        Object.values(errors).forEach( (val) => {
            if(val.length > 0) {
                valid = false;
            }
        });

        return valid;
    }

    submitFormHandler = (event) => {

        event.preventDefault();

        if(this.validateForm()) {

            let formData = {
                name: this.state.name,
                category: this.state.category,
                description: this.state.description,
                price: this.state.price
            };

            let productList = this.state.productList;

            if(this.state.tempEditProductIndex == null){
                //Add new product
                productList.push(formData);
            } else {
                //If it is edit form, replace existing index
                productList.splice(this.state.tempEditProductIndex, 1, formData);
            }

            this.setState({
                productList: productList,
            })

            this.resetForm();
        }
    }

    deleteProduct = (selectedIndex) => {
        let productList = this.state.productList;
        productList.splice(this.state.selectedIndex, 1);

        this.setState({
            productList: productList,
        })
    }

    fillEditForm = (selectedIndex) => {

        let productList = this.state.productList;
        let product = productList.find((product,index) => {
            if(selectedIndex === index){
                return true;
            }
        })

        if (product) {
            this.setState({
                name: product.name,
                category: product.category,
                description: product.description,
                price: product.price,
                tempEditProductIndex: selectedIndex
            });
        }
    }

    resetForm = () => {
        this.setState({
            name: '',
            category: '',
            description: '',
            price: '',
            tempEditProductIndex: null
        })
    }

    /*Ger category name by category id*/
    getCategoryNameById = (categoryId) => {

        let category = this.state.categories.find( function(category){
            if(category.id === parseInt(categoryId)) {
                return true;
            }
        });

        return (category) ? category.name : '';
    }

    render() {
        return (
            <div className="App">
                <Container >
                    <h2>Inventory management system</h2>

                    <div className="col-sm-8">
                        <h4>Add/Edit Form</h4>
                        <form className="form-horizontal product-form ">
                        <div className="form-group">
                            <label className="control-label col-sm-3" htmlFor="email">Product Name*:</label>
                            <div className="col-sm-9">
                                <input type="text"
                                       className="form-control"
                                       name="name"
                                       value={this.state.name}
                                       onChange={this.changeHandler} />
                                       {/*onChange={(event) => this.setState({
                                           name: event.target.value
                                       })
                                       } />*/}

                                {this.state.errors.name.length > 0 &&
                                <span className='error'>{this.state.errors.name}</span>}

                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-sm-3" htmlFor="category">Category*:</label>
                            <div className="col-sm-9">
                                <select
                                    name="category"
                                    className="form-control"
                                    value={this.state.category}
                                    onChange={this.changeHandler} >
                                    <option value="">Select</option>
                                    {this.state.categories.map((category,index) =>
                                        <option value={category.id} key={index}>{category.name}</option>
                                    )}
                                </select>

                                {this.state.errors.category.length > 0 &&
                                <span className='error'>{this.state.errors.category}</span>}
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-sm-3" htmlFor="description">Description:</label>
                            <div className="col-sm-9">
                                <input type="text"
                                       className="form-control"
                                       name="description"
                                       value={this.state.description}
                                       onChange={this.changeHandler} />
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label col-sm-3" htmlFor="description">Price*:</label>
                            <div className="col-sm-9">
                                <input type="text"
                                       className="form-control"
                                       name="price"
                                       value={this.state.price}
                                       onChange={this.changeHandler} />

                                {this.state.errors.price.length > 0 &&
                                <span className='error'>{this.state.errors.price}</span>}

                            </div>
                        </div>

                        <div className="form-group">
                            <div className="col-sm-offset-3 col-sm-9">
                                <button type="submit" onClick={this.submitFormHandler} className="btn btn-success pull-left">Save</button>
                            </div>
                        </div>

                    </form>
                    </div>


                    <div className="col-sm-12">
                        <h4>Items List</h4>
                        <table className="table text-left">
                        <tbody>
                            <tr>
                                <th>Product</th>
                                <th>Category</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>

                            {this.state.productList.map((product,index) =>
                                <tr key={index}>
                                    <td>
                                        {product.name}
                                    </td>
                                    <td>
                                        {this.getCategoryNameById(product.category)}
                                    </td>
                                    <td>
                                        {product.description}
                                    </td>
                                    <td>
                                        ${product.price}
                                    </td>
                                    <td>
                                        <button onClick={() => this.fillEditForm(index) } className="btn btn-primary">Edit</button>
                                        <button onClick={() => this.deleteProduct(index) } className="btn btn-danger">Delete</button>
                                    </td>
                                </tr>
                            )}

                            { (this.state.productList.length === 0 ) &&
                                <tr className="text-center">
                                    <td colSpan="5">
                                        No products
                                    </td>
                                </tr>
                            }
                        </tbody>
                    </table>
                    </div>
                </Container>

            </div>
        )
    }

}

export default App;
